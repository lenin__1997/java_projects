import java.util.*;
import javax.swing.text.html.HTMLDocument.Iterator;

public class Hello{
public static void main(String[] args) {
    System.out.println("Hello");
    ArrayList<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);
    list.add(3);
    for(int i:list){
        System.out.println(i);
    }
    // Iterator a = (Iterator) list.iterator();
    Queue<Integer> q = new LinkedList<>();

    q.add(1);
    q.add(2);
    q.add(3);
    System.out.println("Size is : "+q.size());
    System.out.println("Head is : "+q.peek());
    System.out.println("Removed is : "+q.poll());
    System.out.println("Head is : "+q.peek());

}
}
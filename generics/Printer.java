package generics;

public class Printer <T>{					//Type Parameter 
	
	T thingToPrint;
	
	Printer(T thingToPrint) {				// Constructor
		this.thingToPrint = thingToPrint;
	}
	
	public void print() {					// Method
		System.out.println(thingToPrint);
	}
}

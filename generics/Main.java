package generics;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Printer<Integer> a = new Printer<>(34);
		Printer<Double> b = new Printer<>(34.5);
		Printer<String> c = new Printer<>("Hello");
		Printer<Boolean> d = new Printer<>(true);
		Printer<Character> e = new Printer<>('H');
		
		a.print();
		b.print();
		c.print();
		d.print();
		e.print();
	}

}

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exception {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        try{
    
            System.out.println("Enter a number for A : ");
            int x = scan.nextInt();
            
            System.out.println("Enter a number for B : ");
            int y = scan.nextInt();
    
            int z = x/y;
            System.out.println("Result : "+ z );
        }
        catch(ArithmeticException e){
            System.out.println("You can't devide by zero");
        }
        catch(InputMismatchException e){
            System.out.println("Please enter a Number");
        }
        finally{
            scan.close();
        }
    }
}

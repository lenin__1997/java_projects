import java.util.*;

public class genericCondition{
    public static void main(String[] args) {
        // List interface ArrayList class 
        List<Integer> intList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();

    
        intList.add(4);
        stringList.add("Hello");
        // Call Method
        printArray(intList);
        printArray(stringList);
    }
    // Wild card ?
    public static void printArray(List<?> myList){
        System.out.println(myList+"printed");
    }

}
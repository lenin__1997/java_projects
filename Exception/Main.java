package Exception;
import java.util.*;
public class Main {

	public static void main(String[] args) {
		System.out.println("Hi");
        ArrayList<String> list =new ArrayList<String>();

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Your age");
		int age = scan.nextInt();
		try {
			ageRestriction(age);
		}
		catch(Exception e){
			System.out.println("Problem occured : "+e);
		}
		finally {
			scan.close();
		}
		
	}
	static void ageRestriction(int age)throws ageException{
		if(age<18) {
			throw new ageException("\n"+"You have to be above 18 to signin");
		}
		else {
			System.out.println("You are signed in");
		}
	}
}

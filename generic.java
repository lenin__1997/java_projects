public class generic {
    public static void main(String[] args) {
        Integer[] intArray = {1,2,3,4,5};
        Double[] doubleArray = {5.2, 4.1, 3.6, 2.9, 1.2};
        Character[] charArray = {'A','B','C','D','E'};
        String[] stringArray = {"Eyes","Nose","Ear"};

        // displayArray(intArray);
        // displayArray(doubleArray);
        // displayArray(charArray);
        // displayArray(stringArray);

        System.out.println(getFirst(intArray));
        System.out.println(getFirst(doubleArray));
        System.out.println(getFirst(charArray));
        System.out.println(getFirst(stringArray));
        
    }
    public static <Thing> void displayArray(Thing[] array){
        for(Thing x : array){
            System.out.print(x+" ");
        }
        System.out.println();
    }
    public static <T> T getFirst(T[] array){
        return array[0];
    }
}

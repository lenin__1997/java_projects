package bound;

public class GenericClass <T extends String,S extends Integer> {
	T a;
	S b;
	GenericClass(T a,S b){
		this.a=a;
		this.b=b;
//		System.out.println("Value of A "+a);
//		System.out.println("Value of B "+b);
	}
	public T getValue(){
		return a;
	}
	
}

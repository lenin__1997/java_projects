package bound;

public class Main {

	public static void main(String[] args) {
		
		GenericClass<String,Integer> value = new GenericClass<>("Hello",2);
		
		System.out.println(value.getValue()); 
	}

}
